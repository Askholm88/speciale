import React, { useState, useRef, useEffect } from 'react'
import Test1 from './pages/training'
import Scores from './pages/scoresPage'
import styled from 'styled-components'
import SidebarButton from './components/Button'
import Home from './pages/home'
import { Icon, Menu, Sidebar, Divider, Pagination } from 'semantic-ui-react'
import './App.css'

const StyledContainer = styled.div`
  .focus {
    outline: 2px dotted red;
  }
`

const StyledSidebarButton = styled(SidebarButton)`
  z-index: 1;
  :focus {
    outline: 2px red dotted;
  }
`

const StyledSidebar = styled(Sidebar.Pusher)`
  min-height: 100%;
  display: flex;
`

const StyledContent = styled.div`
  width: 100%;
  display: inline;
  text-align: center;
`

const StyledDivider = styled(Divider)`
  margin-top: 0px !important;
`

const SkipLink = styled.a`
  position: absolute;
  background-color: grey;
  color: blue;
  opacity: 1;
  z-index: 2;
  top: -40px;
  left: 40px;
  :focus {
    top: 0px;
    outline: 2px dotted red;
  }
`

const StyledMenuItem = styled(Menu.Item)`
  align-items: center;
  display: inline-flex;
  width: 100%;
  border: none;
`

const StyledPagination = styled(Pagination)`
  a.item {
    color: white !important;
    border-color: transparent !important;
  }
  a.active.item {
    background-color: #104c7a !important;
    border-color: transparent !important;
  }
`

const PaginationContainer = styled.div`
  display: block;
  text-align: center;
  background-color: #2185d0;
`

export const ContentContext = React.createContext()
export const TaskContext = React.createContext()

function App({ className }) {
  const [visible, setVisible] = useState(false)
  const [content, setContent] = useState(<Home />)
  const menuRef = useRef()
  const sideBarRef = useRef()

  useEffect(() => {
    if (visible === true) {
      menuRef.current.focus()
    }
  }, [visible])


  // roving focus
  const handleMenuTab = (e) => {
    const focusableButtons = sideBarRef.current.ref.current.querySelectorAll(
      'button',
    )
    const firstElement = focusableButtons[0]
    const lastElement = focusableButtons[focusableButtons.length - 1]

    if (e.shiftKey) {
      if (document.activeElement === firstElement) {
        lastElement.focus()
        console.log(focusableButtons)
        e.preventDefault()
      }
    } else {
      if (document.activeElement === lastElement) {
        firstElement.focus()
        e.preventDefault()
      }
    }
  }

  return (
    <StyledContainer>
      <ContentContext.Provider value={(content, setContent)}>
        <SkipLink href="#main-content">Skip to main content</SkipLink>
        <Sidebar.Pushable role="menu">
          <Sidebar
            tabIndex="-1"
            onKeyDown={(e) => {
              if (e.keyCode === 27) {
                setVisible(false)
              }
              if (e.keyCode === 9) {
                handleMenuTab(e)
              }
            }}
            ref={sideBarRef}
            role="navigation"
            as={Menu}
            animation="push"
            icon="labeled"
            inverted
            onHide={() => setVisible(false)}
            visible={visible}
            vertical
            width="thin">
            <StyledMenuItem
              ref={menuRef}
              className="item"
              as="button"
              role="button"
              tabIndex="0"
              onClick={() => setContent(<Home />)}>
              <Icon name="home" />
              Home
            </StyledMenuItem>
            <StyledMenuItem
              className="item"
              as="button"
              role="button"
              tabIndex={0}
              onClick={() => setContent(<Test1 />)}>
              <Icon name="gamepad" />
              Developer Training
            </StyledMenuItem>
            <StyledMenuItem
              className="item"
              as="button"
              role="button"
              tabIndex={0}
              onClick={() => setContent(<Scores />)}>
              <Icon name="chart bar" />
              Achievements & Scoreboard
            </StyledMenuItem>
          </Sidebar>
          <StyledSidebar dimmed={visible}>
            <StyledSidebarButton
              className="sidebarbutton"
              role="button"
              aria-label="open-menu"
              color="grey"
              onClick={() => setVisible(true)}
            />
            <StyledContent role="main">
              <h1 id="main-content">
                Accesibility Training - For Web Developers
              </h1>
              <StyledDivider className={className} />
              {content}
            </StyledContent>
          </StyledSidebar>
        </Sidebar.Pushable>
      </ContentContext.Provider>
    </StyledContainer>
  )
}

export default App
