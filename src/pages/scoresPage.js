import React, { useState, useContext, useEffect } from 'react'
import 'semantic-ui-css/semantic.min.css'
import { Image, List, Grid, Button, Icon } from 'semantic-ui-react'
import styled from 'styled-components'
import { UserContext } from '../contexts/userContext'

//assets
import Robust from '../assets/achievements/robust.png'
import Operable from '../assets/achievements/operable.png'
import Perceivable from '../assets/achievements/perceivable.png'
import Understandable from '../assets/achievements/understandable.png'
import FirstTask from '../assets/achievements/firsttask.png'

import axios from 'axios'

const PageContainer = styled.div`
  display: inline-flex;
  width: 100%;
`

const StyledGrid = styled(Grid)`
`

const StyledList = styled(List)`
  width: 50% !important;
  margin: 15px !important;
  max-height: 80vh;
  text-align: left;
  overflow-y: hidden;
`
const StyledGridItem = styled(Grid.Column)`
  text-align: center;
  align-self: center;
`

const StyledAchievement = styled(Image)`
  display: inline-flex !important;
`

const StyledIcon = styled(Icon)`
  display: inline-block !important;
`

const AchievementContainer = styled.div`
  display: inline-block;
  width: 50%;
`

const StyledListItem = styled(List.Item)`
  vertical-align: middle;
`

const Scores = ({ checked, className }) => {
  const [scores, setScores] = useState([])
  const { user, dispatch } = useContext(UserContext)

  const achievements = [
    {
      picture: FirstTask,
      text: <p>Finish the first task</p>,
      obtained: user.taskScores.task1 === '1',
    },
    {
      picture: Perceivable,
      text: <p>Finish Perceivable part</p>,
      obtained: user.taskScores.task1 === '1',
    },
    {
      picture: Operable,
      text: <p>Finish Operable part</p>,
      obtained: user.taskScores.task2 === '1',
    },
    {
      picture: Understandable,
      text: <p>Finish Understandable part</p>,
      obtained: user.taskScores.task3 === '1',
    },
    {
      picture: Robust,
      text: <p>Finish Robust part</p>,
      obtained: user.taskScores.task4 === '1',
    },
  ]
  useEffect(() => {
    const result = async () => {
      await axios
        .get('http://localhost:3001/scores')
        .then((response, error) => {
          if (error) {
            console.log(error)
          } else {
            setScores(response.data.scores)
          }
        })
    }
    result()
  }, [])

  console.log(scores)
  return (
    <PageContainer>
        <AchievementContainer>
          <h2>Your achievements</h2>
      <StyledGrid stackable celled columns={3}>
        {achievements.map((current, index) => (
          <StyledGridItem>
            {current.obtained ? (
              <StyledAchievement src={current.picture} />
            ) : (
              <StyledAchievement
                src={current.picture}
                style={{ filter: 'grayscale(100%)' }}
              />
            )}
            {current.text}
          </StyledGridItem>
        ))}
      </StyledGrid>
      </AchievementContainer>
      <StyledList
        className={className}
        divided
        ordered
        verticalAlign="middle"
        size="big">
        <h2>Scoreboard</h2>
        {scores &&
          scores.map((current, index) => {
            return (
              <StyledListItem key={current.username}>
                <List.Content floated="right">
                  <p>Score: {current.added_score}</p>{' '}
                </List.Content>
                {current.username === user.name && (
                  <StyledIcon
                    className={className}
                    size="big"
                    color="green"
                    name="hand point right"
                  />
                )}
                <Image
                  avatar
                  src="https://react.semantic-ui.com/images/avatar/small/lena.png"
                />
                <List.Content>{current.username}</List.Content>
              </StyledListItem>
            )
          })}
      </StyledList>
    </PageContainer>
  )
}

export default Scores
