import React, {
  useRef,
  useState,
  useReducer,
  useEffect,
  useContext,
} from 'react'
import CardButton from '../components/CardButton'
import styled from 'styled-components'
import Modal from '../components/Modal'
import { userReducer } from '../Reducers/userReducer'
import { Form, Button, Tab, Image } from 'semantic-ui-react'
import { UserContext } from '../contexts/userContext'

//assets
import Developer from '../assets/images/developer.jpg'
import Designer from '../assets/images/designer.jpg'

const StyledCardButton = styled(CardButton)``

const StyledForm = styled(Form)`
  margin: 25px;
  display: inline-block;
  .Form.Input {
    display: block !important;
  }
`

const StyledCardContainer = styled.div`
  display: inline-flex;
`

const StyledButton = styled(Button)`
  position: absolute;
  top: 0px;
  right: 0px;
  font-size: 1.4em !important;
  :focus {
    outline: 2px dotted red !important;
  }
`
const StyledModal = styled(Modal)`
  background-color: lightgrey !important;
  height: 400px;
  width: 600px;
`
const StyledHeader = styled.h1`
  margin: 25px;
  display: inline-block;
`

const StyledTab = styled(Tab)`
  .active.item {
    background-color: lightgrey !important;
  }
  .item {
    background-color: grey !important;
  }
`
const StyledProfile = styled.div`
  display: inline-grid;
`

const Home = () => {
  const [open, setOpen] = useState(true)
  const signupRef = useRef(null)
  const { user, dispatch } = useContext(UserContext)
  const { isLoggedIn, name, picture, addedScore, taskScores } = user

  const ModalContent = ({ ref, active, dispatch }) => {
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')

    const handleNameChange = (e, value) => {
      setName(value)
    }

    const handlepasswordChange = (e, value) => setPassword(value)

    const handleSignIn = (value) => {
      console.log(value)
      dispatch({ type: `LOGIN`, name, password, setOpen })
    }

    const handleSignUp = (value) => {
      console.log(value)
      dispatch({ type: `CREATE_USER`, name, password, setOpen })
    }
    console.log(isLoggedIn, name, picture)
    return (
      <>
        <StyledButton
          aria-label="close"
          floated="right"
          color="black"
          onClick={() => setOpen(false)}>
          X
        </StyledButton>
        <StyledHeader>Please enter you name and password here.</StyledHeader>
        <StyledForm>
          <Form.Group>
            <Form.Input
              placeholder="Name"
              name="name"
              onChange={(e, value) => handleNameChange(e, value.value)}
            />
            <Form.Input
              placeholder="Password"
              name="password"
              onChange={(e, value) => handlepasswordChange(e, value.value)}
            />
            {active === '1' ? (
              <Form.Button color="green"
                onClick={(e, value) => handleSignIn(value)}
                content={'Sign In'}
              />
            ) : (
              <Form.Button color="blue"
                onClick={(e, value) => handleSignUp(value)}
                content={'Signup'}
              />
            )}
          </Form.Group>
        </StyledForm>
      </>
    )
  }

  const panes = [
    {
      menuItem: 'Sign in',
      render: () => (
        <ModalContent
          active="1"
          ref={signupRef}
          dispatch={dispatch}
          user={user}
        />
      ),
    },
    {
      menuItem: 'Signup',
      render: () => (
        <ModalContent
          active="2"
          ref={signupRef}
          dispatch={dispatch}
          user={user}
        />
      ),
    },
  ]
  console.log(user)
  const progress =
  (Object.values(taskScores).filter(current => current > 0).length)/4*100
  const achievements = Object.values(user.taskScores).filter(
    (current) => current === '1',
  )
  return (
    <StyledCardContainer>
      <Image src="https://react.semantic-ui.com/images/avatar/small/lena.png" />
      {user.isLoggedIn && (
        <StyledProfile>
          <h2>
            Hello <i>{user.name}</i> how are you today?
          </h2>
          <p>
            Progress: {progress}% <br />
            Achievements: {achievements.length}/5 <br />
            Total score: {user.addedScore}{' '}points
          </p>
        </StyledProfile>
      )}
      {!user.isLoggedIn && (
        <StyledModal
          size="small"
          setOpen={setOpen}
          open={open}
          tabIndex="-1"
          onKeyDown={(e) => {
            if (e.keyCode === 27) {
              setOpen(false)
            }
          }}
          aria-modal="true"
          content={<StyledTab panes={panes} />}
          open={open}
        />
      )}
    </StyledCardContainer>
  )
}

export default Home
