import React, {
  Fragment,
  useState,
  useReducer,
  useEffect,
  useRef,
  useContext,
} from 'react'
import 'semantic-ui-css/semantic.min.css'
import styled from 'styled-components'
import SplitPane, { Pane } from 'react-split-pane'
import PointsCounter from '../components/PointsCounter'
import { LiveProvider, LiveEditor, LiveError, LivePreview } from 'react-live'
import { Button, Pagination } from 'semantic-ui-react'
import Modal from '../components/Modal'
import Image from '../components/Image'
import ColorTestImage from '../components/ColorBlindImage'
import SidebarButton from '../components/Button'
import { answerReducer } from '../Reducers/answerReducer'
import { scoresReducer } from '../Reducers/scoresReducer'
import axios from 'axios'
import {
  GetDescription,
  GetCode,
  GetEffect,
  GetScore,
} from '../components/Tasks/taskGetter'
import { UserContext } from '../contexts/userContext'
import { ListItem } from '../components/Tasks/Task2/ListItem'
import ImageComponent from '../components/Image'
import ImageCheckbox from '../components/ImageCheckbox'
import {
  Header,
  Icon,
  Menu,
  Segment,
  Sidebar,
  Dropdown,
} from 'semantic-ui-react'
import Robust from '../assets/achievements/robust.png'
import Operable from '../assets/achievements/operable.png'
import Perceivable from '../assets/achievements/perceivable.png'
import Understandable from '../assets/achievements/understandable.png'
import FirstTask from '../assets/achievements/firsttask.png'

const StyledHeader = styled.div`
  background-color: #2185d0;
  margin: 0px;
`

const SidebarContainer = styled.div`
  .div {
    outline: 2px red dotted;
  }
  #dropdown:hover > div {
    display: block;
    background-color: pink;
  }
  #dropdown:focus-within > div {
    display: block;
  }
  width: 20px;
  :focus-within,
  :hover {
    transition: 0.3s;
    width: 200px;
  }
  height: 107%;
  position: absolute;
  top: 0;
  left: -11px;
  z-index: 1;
  background: #555;
  color: #fff;
  overflow: hidden;
  a {
    color: Blue;
  }
`
const StyledFooter = styled.div`
  display: block;
  text-align: center;
  position: absolute;
  min-height: 15%;
  width: 100%;
  bottom: -15px;
  right: 0px;
  background-color: teal;
`

const StyledModalContainer = styled.div`
  height: inherit;
`

const StyledModalPane = styled(Pane)`
  min-height: inherit;
`

const StyledSidebar = styled(Sidebar.Pusher)`
  min-height: 100%;
  display: flex;
`

const StyledContent = styled.div`
  display: inline;
  text-align: center;
`

const StyledSidebarButton = styled(SidebarButton)`
  z-index: 1;
  :focus {
    outline: 2px red dotted;
  }
`

const StyledSplitPane = styled(SplitPane)`
  .App {
    font-family: sans-serif;
    text-align: center;
  }
  .Resizer {
    background: #000;
    opacity: 0.2;
    z-index: 1;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    -moz-background-clip: padding;
    -webkit-background-clip: padding;
    background-clip: padding-box;
  }

  .Resizer:hover {
    -webkit-transition: all 2s ease;
    transition: all 2s ease;
  }

  .Resizer.horizontal {
    height: 11px;
    margin: -5px 0;
    border-top: 5px solid rgba(255, 255, 255, 0);
    border-bottom: 5px solid rgba(255, 255, 255, 0);
    cursor: row-resize;
    width: 100%;
  }

  .Resizer.horizontal:hover {
    border-top: 5px solid rgba(0, 0, 0, 0.5);
    border-bottom: 5px solid rgba(0, 0, 0, 0.5);
  }

  .Resizer.vertical {
    width: 11px;
    margin: 0 -5px;
    border-left: 5px solid rgba(255, 255, 255, 0);
    border-right: 5px solid rgba(255, 255, 255, 0);
    cursor: col-resize;
  }

  .Resizer.vertical:hover {
    border-left: 5px solid rgba(0, 0, 0, 0.5);
    border-right: 5px solid rgba(0, 0, 0, 0.5);
  }
  .Resizer.disabled {
    cursor: not-allowed;
  }
  .Resizer.disabled:hover {
    border-color: transparent;
  }
`

/* const ButtonText = styled.div`
  display: flex;
  flex-flow: column;
  .button {
    margin-top: 20px;
  }
`

const Hint = styled.p`
  font-weight: 600;
  margin-bottom: 50px;
`

const TaskContainer = styled.div`
  flex-grow: 1;
` */

const StyledButton = styled(Button)`
  font-size: 1.4em !important;
  :focus {
    outline: 2px dotted red !important;
  }
  position: relative;
  z-index: 2;
`

/* const taskdescription = () => {
  return (
    <TaskContainer>
      <h1>Task 1 - Introduction</h1>
      <br />
      <p>
        The main task of this webapp is to train developers to implement
        accessibility into their develoment, so that accessibility doesn’t
        become a chore but an automatic gesture, when developing. The app let’s
        you see the code for the webpage to the far right, but in a
        representation, that emulates a certain disability. Each task is
        designed so that the developer needs to find something specific in the
        webpage, while being influenced by the disability simulation.
      </p>

      <br />
      <br />
      <Hint>Hint - hints will be shown here in each task!</Hint>

      <ButtonText>
        <strong>OBS:</strong>You will need a screen reader for several tasks.
        You can click the button below to go to the download page of NVDA screen
        reader, one of the most popular screen readers available.
        <StyledButton
          color="blue"
          size="large"
          fluid
          href="https://www.nvaccess.org/download/">
          Go to NVDA download page!
        </StyledButton>
      </ButtonText>
    </TaskContainer>
  )
}
 */
const WebsitePane = styled(Pane)`
  font-size: 1.25em;
  line-height: 1.35;
  margin-left: 10px;
  margin-right: 10px;
  display: flex;
  flex-flow: column;
  height: 85%;
  justify-content: space-between;
`

const StyledLiveError = styled(LiveError)`
  color: white;
  height: 30%;
  background-color: black;
`

const StyledLiveEditor = styled(LiveEditor)`
  height: 100%;
`

const StyledLivePreview = styled(LivePreview)`
  padding-left: 15px;
  padding-right: 0px;
  height: 90%;
  text-align: left !important;
`

const PaginationContainer = styled.div`
  display: block;
  text-align: center;
  background-color: #2185d0;
  a.item {
    color: white !important;
    border-color: transparent !important;
  }
  a.active.item {
    background-color: #104c7a !important;
    border-color: transparent !important;
  }
`

const TaskPane = styled(Pane)`
  margin-left: 30px;
  padding: 15px;
  padding-top: 0px;
  line-height: 1.35;
  font-size: 1.25em;
  text-align: left;
  display: flex;
  height: 88%;
  flex-direction: column;
`

const CodePane = styled(Pane)`
  height: 100%;
  background-color: black;
  caret-color: white;
`

const FilterSpan = styled.span`
  height: 100%;
`

/* const codedummy = () => {
  return `
<>
<p>The webpage will be displayed in this column</p>
<h1>This is the Webpage</h1>
<br/>
<p>See if you can solve the task explained in column 1</p>
<p>You will be awarded points based on how fast you solve the task.</p>
<p><strong>Points are awarded as follows</strong></p>
<ul>
<li>100 starting points</li>
<li>-1 point per minute spent</li>
<li>-1 point per 10 characters spent correcting the code</li>
</ul>
${checkboxes
  .map(
    (current, index) =>
      `<input type="checkbox" defaultChecked={${current}} id="${index}" onClick={()=>changeState(${index})} />
      <Image motive="${index}" />
      `,
  )
  .join('')}

</>
`
}
 */

const MouseCover = styled.div`
  z-index: 2;
  position: absolute;
  min-height: 110%;
  width: 110%;
  top: -20px;
  left: -30px;
  background-color: brown;
  opacity: 0.05;
`

const PointsDisplay = styled.div`
  color: white;
  font-size: 2rem;
  font-family: Bookman;
  background-color: green;
  padding-bottom: 5px;
  padding-top: 5px;
  :hover {
    color: red;
  }
`

const HeaderGrid = styled.div`
  border: 2px solid black;
  width: 90%;
`

const Red = styled.div`
  background-color: black;
  min-height: 50%;
`

const Green = styled.div`
  text-align: center;
  font-size: 2rem;
`
const StyledAchievement = styled.img`
  display: inline-flex !important;
  width: 50px;
  transition: 3s;
  width: 500px;
`

const SimHeader = styled.div`
  display: inline;
  font-size: 2rem;
`

const dummyCode = (checkboxes) => {
  return `
  <>
  <h1>Does alt text matter?</h1>
  <p>By giving images an alt text it can be targeted by a screenreader <br/> and the image can be made accessible to, say a blind user.</p>
  <p>Alt texts aren't actually just used for screen readers, but for <strong>SEO optimization</strong> and
  <strong> Image searches</strong>, so there is no reason for skipping on this one.</p>
  <p>Remember to enable the screen reader before clicking Start Task</p>
  
  ${checkboxes
    .map(
      (current, index) =>
        `<Image motive="${Math.floor(Math.random() * 10)}" />
        `,
    )
    .join('')}
        </>
        `
}

const achievements = [
  {
    picture: FirstTask,
    text: <p>Finish the first task</p>,
  },
  {
    picture: Perceivable,
    text: <p>Finish Perceivable part</p>,
  },
  {
    picture: Operable,
    text: <p>Finish Operable part</p>,
  },
  {
    picture: Understandable,
    text: <p>Finish Understandable part</p>,
  },
  {
    picture: Robust,
    text: <p>Finish Robust part</p>,
  },
]

const Test1 = ({ className }) => {
  const [taskId, setTaskId] = useState(1)
  const [filter, setFilter] = useState(GetEffect(taskId))
  const [code, setCode] = useState(GetCode(taskId))
  const [open, setOpen] = useState(false)
  const [taskPoints, setTaskPoints] = useState('')
  const modalRef = React.useRef()
  const [state, dispatch] = useReducer(answerReducer, {
    taskId: taskId,
    ref: modalRef,
    pointsModalOpen: false,
  })
  const [lol, scoreDispatch] = useReducer(scoresReducer, {})

  const { user } = useContext(UserContext)
  const { isLoggedIn, name, score } = user
  const handleClick = (state) => {
    console.log('taskpoints', user, taskPoints)
    dispatch({ type: `SET_SCORES`, user, taskPoints})
  }

  useEffect(() => {
    if (isLoggedIn) {
      const result = async () => {
        await axios
          .get(`http://localhost:3001/scores/${name}`)
          .then((response, error) => {
            if (error) {
              console.log(error)
            } else {
              const scores = response.data.scores
              console.log(scores)
              setTaskPoints(scores) // what the new score in postgres will be set to. Need to update as tasks get done
            }
          })
      }
      result()
    }
  }, [])

  const PointsModal = ({ points }) => {
    return (
      <>
        <StyledButton
          aria-label="close"
          floated="right"
          color="black"
          onClick={() => {
            setOpen(false)
            state.pointsModalOpen = false
          }}>
          X
        </StyledButton>
        <h1>Congratulations you scored: 250 points!</h1>
        {achievements.map(
          (current, index) =>
            index === taskId && <StyledAchievement src={current.picture} />,
        )}
      </>
    )
  }
  const RefModal = React.forwardRef((props, ref) => (
    <StyledModalContainer ref={ref}>
      <StyledLivePreview taskId={taskId} style={filter} />
    </StyledModalContainer>
  ))

  const ModalContent = ({ ref, mouseCover }) => (
    <>
      <StyledModalPane className={className} label="preview">
        {mouseCover === 2 && <MouseCover />}
        <StyledButton
          aria-label="close"
          floated="right"
          color="black"
          onClick={() => setOpen(false)}>
          X
        </StyledButton>
        <RefModal ref={modalRef} />
      </StyledModalPane>
      <StyledButton
        color="blue"
        size="big"
        fluid
        onClick={() => {
          dispatch({ type: `CHECK_ANSWER ${taskId}`, ref: modalRef, user, taskPoints })
        }}>
        Check Answer
      </StyledButton>
    </>
  )

  console.log()
  return (
    <>
      <Modal
        open={state.pointsModalOpen}
        setOpen={setOpen}
        content={<PointsModal points={taskPoints} />}
      />
      <PaginationContainer role="navigation">
        <Pagination
          activePage={taskId}
          firstItem={null}
          lastItem={null}
          onPageChange={(e, value) => {
            setTaskId(value.activePage)
            setCode(GetCode(value.activePage))
            setFilter(GetEffect(value.activePage))
          }}
          pointing
          secondary
          totalPages={4}
        />
      </PaginationContainer>
      <LiveProvider
        code={code}
        scope={{
          Image,
          Sidebar,
          Menu,
          Dropdown,
          ListItem,
          StyledHeader,
          SidebarContainer,
          StyledSidebarButton,
          StyledSidebar,
          Segment,
          Icon,
          Header,
          StyledContent,
          StyledFooter,
          ColorTestImage,
        }}>
        <Modal
          setOpen={setOpen}
          open={open}
          tabIndex="-1"
          onKeyDown={(e) => {
            if (e.keyCode === 27) {
              setOpen(false)
            }
          }}
          aria-modal="true"
          size="large"
          content={<ModalContent ref={modalRef} mouseCover={taskId} />}
          open={open}
        />

        <StyledSplitPane split="vertical" minSize={600}>
          <TaskPane className={className} label="description">
            {<GetDescription taskId={taskId} />}
          </TaskPane>
          <StyledSplitPane split="vertical" defaultSize={600}>
            <CodePane className={className} label="code-input">
              <StyledLiveEditor onChange={(e) => setCode(e)} />
            </CodePane>
            <WebsitePane className={className} label="preview">
              <StyledLiveError />
              <StyledLivePreview taskId={taskId} />
              <StyledButton
                color="blue"
                size="big"
                fluid
                onClick={() => {
                  setOpen(true)
                }}>
                Start Task
              </StyledButton>
            </WebsitePane>
          </StyledSplitPane>
        </StyledSplitPane>
      </LiveProvider>
    </>
  )
}

export default Test1
