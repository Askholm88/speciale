import React, { createContext, useState } from 'react'
import Test1 from '../pages/test1'
import Test2 from '../pages/test2'
import Home from '../pages/home'
import App from '../App'

export const TaskContext = createContext()

const initialState =  () => <Test1 />

export const TaskContextProvider = ({ content }) => {
    const [currentTask, setCurrentTask] = useState(initialState)
    return (
        <TaskContext.Provider value={{ currentTask, setCurrentTask }}>
         {currentTask}
        </TaskContext.Provider>
    )
}
 
export default TaskContext