import React, { useReducer } from 'react'
import { answerReducer } from '../components/Reducers/taskReducer'

export const CheckAnswer = React.createContext()

const CheckAnswerProvider = (props) => {
    const [answer, dispatch] = useReducer(answerReducer, [true, false, false, false, false])
    console.log('answerContext')
return(
    <CheckAnswer.Provider value={answer, dispatch}>
        {props}
    </CheckAnswer.Provider>
)} 
export default CheckAnswerProvider
