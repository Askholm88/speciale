import React, { useReducer, useState, useContext, createContext } from 'react'
import { userReducer } from '../Reducers/userReducer'
import App from '../App'

export const UserContext = createContext()

export const UserContextProvider = () => {
    const [user, dispatch] = useReducer(userReducer, {
        name: null,
        isLoggedIn: false,
        picture: '',
        taskScores: {},
        addedScore: '',
        achievements: {},
    })

    return (
        <UserContext.Provider value={{ user, dispatch }}>
            <App />
        </UserContext.Provider>
    )
}