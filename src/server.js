const http = require('http')
const app = require('./express')

const port = process.env.PORT || 5432

const server = http.createServer(app)

server.listen(port)