import React from 'react'
import 'semantic-ui-css/semantic.min.css'
import styled from 'styled-components'


// assets
import Rhino from '../../assets/images/rhino.jpg'
import Donkey from '../../assets/images/donkey.jpg'
import Gorilla from '../../assets/images/gorilla.jpg'
import Cows from '../../assets/images/cows.jpg'
import Buffalo from '../../assets/images/buffalo.jpg'
import Sheep from '../../assets/images/sheep.jpg'
import Squirrel from '../../assets/images/squirrel.jpg'
import Kitten from '../../assets/images/kitten.jpg'
import Hen from '../../assets/images/hen.jpg'
import Tiger from '../../assets/images/tiger.jpg'
import { Checkbox } from 'semantic-ui-react'

const motives = [{source: Squirrel, name: '0'}, {source: Kitten, name: '1'}, {source: Hen, name: '2'}, {source: Tiger, name: '3' }, {source: Donkey, name: '4'}, {source: Buffalo, name: '9'}, {source: Gorilla, name: '7'}, {source: Cows, name: '6'}, {source: Rhino, name: '8' }, {source: Sheep, name: '5'} ]

const StyledCheckbox = styled.input`
    display: initial;
    z-index: 1;
    position: relative;
    left: 15px;
    margin-bottom: 5px;
    background: url(${props => props.source});
`

const StyledLabel = styled.label`
    display: inline-flex;
    color: transparent;
    width: 200px;
    height: 200px;
    position: relative;
    margin-right: 5px;
    background: url(${props => props.source});
`

const Container = styled.div`
    display: inline-flex;
`

const ImageComponent = ({ motive, alt }) => {

    return(
        <>
            {motives.map((current) => 
                motive===current.name && (
                    <Container >
                    <StyledCheckbox type="checkbox" aria-labelledby={alt} source={current.source} id={current.name} />
                <StyledLabel htmlFor={current.name} source={current.source}>{alt}</StyledLabel>              
                </Container>
                ) 
            )}
        </>
    )
}

export default ImageComponent