import React from 'react'
import styled from 'styled-components'
import 'semantic-ui-css/semantic.min.css'

//assets
import Test1 from '../../assets/images/colorblind1.png'
import Test2 from '../../assets/images/colorblind2.png'
import Test3 from '../../assets/images/colorblind3.jpg'
import Test4 from '../../assets/images/colorblind4.png'
import Test5 from '../../assets/images/colorblind5.jpg'
import Test6 from '../../assets/images/colorblind6.jpg'
import Test7 from '../../assets/images/colorblind7.jpg'
import Test8 from '../../assets/images/colorblind8.jpg'
import Test9 from '../../assets/images/colorblind9.jpg'

const StyledImage = styled.img`
background-image: url(${props => props.source});
display: inline-flex;
color: transparent;
width: 200px;
height: 200px;
position: relative;
margin-right: 5px;
`

const TestImage = ({ motive, title }) => {
    const motives = [{source: Test1, name: '0'}, {source: Test2, name: '1'}, {source: Test3, name: '2'}, {source: Test4, name: '3' }, {source: Test5, name: '4'}, {source: Test6, name: '5'}, {source: Test7, name: '6'}, {source: Test8, name: '7'}, {source: Test9, name: '8'}, {source: Test5, name: '9'}]

    return (
        <>
        {motives.map((current) =>
                motive===current.name && (<>
                <StyledImage src={current.source} title={title} id={current.name} /> 
                  
                </>) 
            )}
        </>
    )
}

export default TestImage



