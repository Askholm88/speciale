import React, { useRef } from 'react'
import 'semantic-ui-css/semantic.min.css'
import { Modal } from 'semantic-ui-react'
import styled from 'styled-components'

const StyledModal = styled(Modal)`
  text-align: center !important;
  min-width: 700px;
  min-height: 700px;
`

const AnswerModal = ({ open, content, setOpen, size, className }) => {
  const modalRef = useRef()

  const handleTabKey = (e) => {
    const focusableModalElements = modalRef.current.ref.current.querySelectorAll(
      'a[href], button, textarea, input[type="text"], input[type="radio"], input[type="checkbox"], select',
    )
    const firstElement = focusableModalElements[0]
    const lastElement =
      focusableModalElements[focusableModalElements.length - 1]

    if (e.shiftKey) {
      if (document.activeElement === firstElement) {
        lastElement.focus()
        e.preventDefault()
      }
    } else {
      if (document.activeElement === lastElement) {
        firstElement.focus()
        e.preventDefault()
      }
    }
  }

  return (
    <>
      <StyledModal
        className={className}
        ref={modalRef}
        tabIndex="-1"
        onKeyDown={(e) => {
          if (e.keyCode === 27) {
            setOpen(false)
          }
          if (e.keyCode === 9) {
            handleTabKey(e)
          }
        }}
        aria-modal="true"
        size={size}
        content={content}
        open={open}
      />
    </>
  )
}

export default AnswerModal
