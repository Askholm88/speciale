import React from 'react'
import styled from 'styled-components'
import 'semantic-ui-css/semantic.min.css'
import { Card, Image } from 'semantic-ui-react'

const StyledContainer = styled.div`
  margin: 50px;
  height: fit-content;
`

    const CardButton = ( { className, image, text, subText, href }) => 
    <StyledContainer className={className}>
      <a href={href}> 
        <Card className={className}>
        <Image src={image} wrapped ui={false} />
        <Card.Content>
          <Card.Header>{text}</Card.Header>
          <Card.Description>
            {subText}
          </Card.Description>
        </Card.Content>
      </Card>  
      </a>
      </StyledContainer> 
    
    
    export default CardButton

