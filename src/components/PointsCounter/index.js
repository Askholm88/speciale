import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import 'semantic-ui-css/semantic.min.css'

const Points = styled.p`
    color: green;
    font-size: 2rem;
`

const PointsCounter = ({ maxPoints }) => {
    const [points, setPoints] = useState(maxPoints)

    useEffect(() => {
        setInterval(() => {
            setPoints((points) => points -1)
        }, 1000)
    }, [])

    return(
    <Points>Points: {points}</Points>
    )
}

export default PointsCounter