import React from 'react'
import 'semantic-ui-css/semantic.min.css'
import styled from 'styled-components'
import { Button } from 'semantic-ui-react'

const StyledButton = styled(Button)`
  flex-flow: column;
  height: 100vh;
`

const SideBarButton = ({ onClick, color, className }) => (
  <StyledButton className={className} onClick={onClick} color={color} icon="sidebar" aria-label="Sidebar" />    
)

export default SideBarButton
