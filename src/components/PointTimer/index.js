import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import 'semantic-ui-css/semantic.min.css'

const PointCounter = ({ maxPoints }) => {
  const [points, setPoints] = useState(maxPoints)

  //   const pointsLeft = () => {
  //     const newPoints = points - 1
  //     return newPoints
  //   }

  useEffect(() => {
    setInterval(() => {
      setPoints((points) => points - 1)
    }, 100)
  }, [])

  return <h1>Points: {points > 0 ? points : <h1>Time's up!</h1>}</h1>
}

export default React.memo(PointCounter)
