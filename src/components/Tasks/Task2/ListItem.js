import React, { useReducer } from 'react'
import 'semantic-ui-css/semantic.min.css'
import styled from 'styled-components'
import { answerReducer } from '../../../Reducers/answerReducer'

export const ListItem = ({ handleClick }) => {

  const items = [
    { text: 'HomePage' },
    { text: 'SubPage 1' },
    { text: 'SubPage 2' },
    { text: 'Goal!' },
    { text: 'SubPage 3' },
  ]
  const FocusDiv = styled.div`
    display: none;
    :focus {
      outline: 2px red dotted;
    }
    a {
      display: block;
    }
  `

  return (
    <FocusDiv>
      {items.map((current, index) =>
        index === 3 ? (
        <label htmlFor="goal">
            {current.text}
            <input id="goal" type="checkbox" />
          </label>
        ) : (
          <a href="#">{current.text}</a>
        ),
      )}
    </FocusDiv>
  )
}
