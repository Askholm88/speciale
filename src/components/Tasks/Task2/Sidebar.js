import React, { useState, useEffect } from 'react'
import 'semantic-ui-css/semantic.min.css'
import styled from 'styled-components'
import SidebarButton from '../../Button'
import { Header, Icon, Image, Menu, Segment, Sidebar } from 'semantic-ui-react'

const StyledSidebarButton = styled(SidebarButton)`
  z-index: 1;
  :focus {
    outline: 2px red dotted;
  }
`

export const TaskSidebar = () => {

    return (
      <Sidebar.Pushable role="menu">
        <Sidebar
          as={Menu}
          animation='push'
          icon='labeled'
          inverted
          vertical
          visible={true}
          width='thin'
        >
          <Menu.Item as='a'>
            <Icon name='home' />
            Home
          </Menu.Item>
          <Menu.Item as='a'>
            <Icon name='gamepad' />
            Games
          </Menu.Item>
          <Menu.Item as='a'>
            <Icon name='camera' />
            Goal!
          </Menu.Item>
        </Sidebar>
  
        <Sidebar.Pusher>
        <StyledSidebarButton
              className="sidebarbutton"
              role="button"
              aria-label="open-menu"
              color="grey"
            />
          <Segment basic>
            <Header as='h3'>Application Content</Header>
            <Image src='https://react.semantic-ui.com/images/wireframe/paragraph.png' />
          </Segment>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    )
  }