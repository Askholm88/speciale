import React, { useState, useEffect } from 'react'
import 'semantic-ui-css/semantic.min.css'
import styled from 'styled-components'

export const GetDescription = ({ taskId }) => {
    const [description, setDescription] = useState()
    useEffect(() => {
      axios
        .get(`http://localhost:3001/tasks/${taskId}`)
        .then((respons, error) => {
          if (error) {
            console.log(error)
          } else {
            setDescription(respons.data.fetchedTask[0].description)
          }
        })
    }, [taskId])
  
    return (
      <>
        {description ? (
          <TaskContainer>
            <h1>{description.Header}</h1>
            <p>{description.Main2}</p>
            <Objective>
            <strong>Objective: </strong>
            <p>{description.Objective}</p>
            <Hint>Hint: {description.Hint}</Hint>
            </Objective>
            <br/>
            <Fact> <strong>Factbox:</strong><br/>
              {description.Main1}</Fact>
            <br/>
            <ButtonText>
              <strong>OBS: </strong> <br/>
              {description.SubText} <StyledButton color="blue" fluid href={description.ButtonRef}>
                {description.ButtonText}
              </StyledButton>
            </ButtonText>
          </TaskContainer>
        ) : (
          <Loading>loading...</Loading>
        )}
      </>
    )
  }

const taskdescription = () => {
    return (
      <TaskContainer>
        <h1>Task 1 - Introduction</h1>
        <br />
        <p>
          The main task of this webapp is to train developers to implement
          accessibility into their develoment, so that accessibility doesn’t
          become a chore but an automatic gesture, when developing. The app let’s
          you see the code for the webpage to the far right, but in a
          representation, that emulates a certain disability. Each task is
          designed so that the developer needs to find something specific in the
          webpage, while being influenced by the disability simulation.
        </p>
  
        <br />
        <br />
        <Hint>Hint - hints will be shown here in each task!</Hint>
  
        <ButtonText>
          <strong>OBS:</strong>You will need a screen reader for several tasks.
          You can click the button below to go to the download page of NVDA screen
          reader, one of the most popular screen readers available.
          <StyledButton
            color="blue"
            size="large"
            fluid
            href="https://www.nvaccess.org/download/">
            Go to NVDA download page!
          </StyledButton>
        </ButtonText>
      </TaskContainer>
    )
  }