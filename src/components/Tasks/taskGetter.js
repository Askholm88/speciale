import React, { useState, useEffect } from 'react'
import 'semantic-ui-css/semantic.min.css'
import styled from 'styled-components'
import { TaskSidebar } from './Task2/Sidebar'
import axios from 'axios'

const Loading = styled.div`
  text-align: center;
  font-size: 2rem;
  margin: 10px;
`

const ButtonText = styled.div`
  display: inline-block;
  flex-flow: column;
  .button {
    margin-top: 20px;
  }
`

const Hint = styled.p`
  margin-bottom: 50px;
  font-weight: 300;
  font-style: italic;
`

const TaskContainer = styled.div`
  flex-grow: 1;
  padding: 15px;
  padding-right: 5px;
  padding-top: 0px !important;
`

const StyledButton = styled.a`
  :focus {
    outline: 2px dotted red !important;
  }
`
const Objective = styled.div`
  font-size: 1.2rem;
  background-color: lightblue;
  padding: 10px;
  font-weight: 600;
`

const Fact = styled.div`
  background-color: lightgrey;
  padding: 10px;
`

const tasks = [
  {
    taskId: 1,
    header: 'Task 1 – Perceivable, Alt-Text',
    main1:
      'Did you know that according to WHO globally there are 2.2 billion people with vision impairment, most of whom are above 50 years old, as of 2019? This means about every third person in the world suffer from visual impairment. You may have heard about the importance of providing appropriate alt-tags for images regarding accessibility and this is because assistive technology such as screen readers use this information to read aloud for the user, who might be blind. Without this text alternative it can be almost impossible for a blind user to navigate a webpage.',
    main2:
      'In this task you will be presented with a webpage as though you were a blind user. Your job is to figure out how to make the page accessible to a screen reader, so that you can select and check off the correct images using the screen reader plugin and keyboard.',
    objective: 'Choose and check the images of gorillas.',
    hint:
      'Use meaningful alt texts for each picture and an empty alt text for images without important information other than visual information.',
    subText:
      'If you wish to read more about Text Alternatives, click the link below.',
    buttonText: 'Go to WCAG 1.1',
    buttonRef: 'https://www.w3.org/TR/WCAG21/#non-text-content',
    code: (checkboxes) => {
      return `
   <>

   <h1>Does alt text matter?</h1>
   <p>By giving images an alt text it can be targeted by a screenreader <br/> and the image can be made accessible to, say a blind user.</p>
   <p>Alt texts aren't actually just used for screen readers, but for <strong>SEO optimization</strong> and
   <strong> Image searches</strong>, so there is no reason for skipping on this one.</p>
   <p>Remember to enable the screen reader before clicking Start Task</p>
   
   ${checkboxes
     .map(
       (current, index) =>
         `<Image motive="${Math.floor(Math.random() * 10)}" />
         `,
     )
     .join('')}
     <Image motive="7" />
   </>
   `
    },
    effect: { opacity: '0' },
  },
  {
    taskId: 2,
    header: 'Task 2 - Operable, Managing Focus',
    main1: `To make your website as accessible as possible regarding operation, a user needs to be able to access all functionality using only the keyboard.`,
    main2: `One of the things developers tend to overlook is keyboard focus. Some users rely solely on keyboard for navigating a webpage. 
  A good way to check if your website is easy to navigate using a keyboard is to see if the tab-order in which different elements on the page receives focus is logical and efficient. 
  If not there are ways to improve this.`,
    objective: `The website to the right has a navigation bar on the left side. Your job is to navigation bar navigable with keyboard, so that you can focus and press the button labelled
  "Goal" in the submenu of Contact Us`,
    hint:
      'You can use the tabIndex to make a non tabable element receive focus. A tabIndex of 0 inserts an element into the natural tab order.',
    subText:
      'Keyboard Focus is part of the WCAG-guidelines OPERABLE, click the link below to read about Keyboard Focus point 2.1.1',
    buttonText: 'Go to WCAG point 2.1.1',
    buttonRef: 'https://www.w3.org/TR/WCAG21/#keyboard',
    code: () => {
      return `
  <>
  <StyledHeader>
        <button>Home</button>
        <button>About</button>
        <button>Info</button>
  </StyledHeader>
  <h1>
    Navigation
  </h1>
  <SidebarContainer>
  <ul>
  <div>Home</div>
  <div>About</div>
  <div id="dropdown">Contact Us
  <Icon name="angle right" />
  <ListItem>Info</ListItem>
  </div>
  </ul>
  </SidebarContainer>
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.  <br/> Sed ornare, turpis eu aliquam posuere, 
      nisl elit rhoncus libero, non venenatis quam lectus et odio. <br/>Integer commodo felis tortor,
       malesuada varius felis semper id. Donec lobortis ex et nisi efficitur, et feugiat turpis elementum.
      <br/>
       Praesent convallis dolor eu mi feugiat condimentum. Vestibulum sed purus bibendum, <br/>sollicitudin turpis ut, tristique dolor. 
       Quisque ornare sodales quam vitae venenatis. Morbi luctus ante id est bibendum dictum.
      </p>
  <StyledFooter>
  Quisque varius et nunc eu dapibus. <br/> Vestibulum in eros euismod, rhoncus diam sit amet, scelerisque elit.
  </StyledFooter>
  </>
  `
    },
    effect: {},
  },
  {
    taskId: 3,
    header: 'Task 3 - Understandable, Consistent Navigation',
    main1: `To make your website as accessible as possible regarding operation, a user needs to be able to access all functionality using only the keyboard.`,
    main2: `This web page is semantically incorrect. The header tags are not placed in a logical manner so that h1 comes first, then h2 and so forth. People who use assistive technology often use
    headers to navigate through a website therefore we should always use the proper tags in correct order. Never use an h-tag  simply for styling purposes!`,
    objective: `Your objective is to move the h-tags in the code so they are in logical order from h1-h5`,
    hint:
      'You have to move the actual elements in the code, not just change the h-tags',
    subText:
      'Consistent Navigation is part of the WCAG-guidelines UNDERSTANDABLE, click the link below to read about Consistent Navigation point 3.2.3',
    buttonText: 'Go to WCAG point 3.2.3',
    buttonRef: 'https://www.w3.org/TR/WCAG21/#consistent-navigation',
    code: () => {
      return `
  <>

  <h5 id="5">
  Sub Article Header
  </h5>
  <h2 id="2">
  Page Header
  </h2>
  <h1 id="1">
  Website Header
  </h1>
  <h4 id="4">
  Article Header
  </h4>
  <h3 id="3">
  Sub Header
  </h3>

  </>
  `
    },
    effect: {},
  },
  {
    taskId: 4,
    header: 'Task 4 - Robust, Title-Attribute',
    main1: `Did you know that 1 in 12 men suffer from some kind of color blindness? Women are less unlucky with only 1 in 200, but with a population of about 7.8 billion people, that still amounts to almost 40 million people for the women alone!`,
    main2: `Developers can do a lot of things with html out of the box to improve accessibility of their webapps. Using built in attributes and ARIA attributes ensures that information is shown the same way on most browser and devices.`,
    objective: `To the right we see a set of numbers from the Ishihara color blindness test. You´re task is to enter the numbers in order once 'Start Task' has been pressed.`,
    hint:
      'Giving an element a title-attribute makes it appear on hover',
    subText:
      'Compatibility goes under the WCAG-guidelines ROBUST, click the link below to read about Compatible point 4.1',
    buttonText: 'Go to WCAG point 4.1',
    buttonRef: 'https://courses.idrc.ocadu.ca/understandinga11y/4_robust.html',
    code: (colorBlindTests) => {
      return `
  <>
  ${colorBlindTests
    .map(
      (current, index) =>
        `<ColorTestImage motive="${Math.floor(Math.random() * 10)}" />
        `,
    )
    .join('')}
    <input type="text" placeholder="enter order here!"/>

  </>
  `
    },
    effect: { filter: 'grayscale(100%)' },
  },
]

export const GetDescription = ({ taskId }) => {
  const [description, setDescription] = useState({})

  useEffect(() => {
    setDescription(tasks.find((task) => task.taskId === taskId))
  }, [taskId])

  return (
    <>
      {description ? (
        <TaskContainer>
          <h1>{description.header}</h1>
          <p>{description.main2}</p>
          <Objective>
            <strong>Objective: </strong>
            <p>{description.objective}</p>
            <Hint>Hint: {description.hint}</Hint>
          </Objective>
          <br />
          <Fact>
            {' '}
            <strong>Factbox:</strong>
            <br />
            {description.main1}
          </Fact>
          <br />
          <ButtonText>
            <strong>OBS: </strong> <br />
            {description.subText}{' '}
            <StyledButton color="blue" fluid href={description.buttonRef}>
              {description.buttonText}
            </StyledButton>
          </ButtonText>
        </TaskContainer>
      ) : (
        <Loading>loading...</Loading>
      )}
    </>
  )
}

export const GetCode = (taskId) => {
  const checkboxes = [false, false, false, false, false, false, false, false]
  const taskObject = tasks.find((task) => task.taskId === taskId)
  return taskObject.code(checkboxes)
}

export const GetEffect = (taskId) => {
  const taskObject = tasks.find((task) => task.taskId === taskId)
  return taskObject.effect
}

export const GetScore = async (taskId) => {
  const scores = await axios
    .get('http://localhost:3001/scores')
    .then((response, error) => {
      if (error) {
        console.log(error)
      } else {
        return response.data.scores
      }
      return scores
    })
}
/* 
export const SetTaskScore = async (taskId, taskScores) => {
  const taskScore = await axios.post(`http://localhost:3001/scores/${username}/`).then((response, error) => {
    if (error) {
      console.log(error)
    } else {
      console.log(taskScore)
    }
  })
} */
