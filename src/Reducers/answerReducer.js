import React, { useContext } from 'react'
import { UserContext } from '../contexts/userContext'
import Modal from '../components/Modal'
import isEqual from 'lodash/isEqual'
import axios from 'axios'

export const answerReducer = (state, action) => {
  switch (action.type) {
    case `CHECK_ANSWER 1`:
      const checkboxes = state.ref.current.querySelectorAll('input')
      const checkboxArr = Array.prototype.slice.call(checkboxes)
      const gorillas = checkboxArr.filter((current) => {
        if (current.id === '7') {
          return current
        }
      })
      const checkedGorillas = gorillas.filter((current) => {
        if (current.checked) {
          return current
        }
      })
      console.log('gorillas', checkboxArr)
      console.log('checkedgorillas', checkedGorillas)
      if (gorillas.length === checkedGorillas.length) {
        const newState = {...state, pointsModalOpen: true}
        if (action.user.isLoggedIn) {
          const newTaskPoints = {...action.taskPoints}
          newTaskPoints.task1="1"
          const addedScore = Object.values(newTaskPoints).filter(current => current > 0).length*250
          axios.post(`http://localhost:3001/scores/${action.user.name}`, { "taskPoints": newTaskPoints, "addedScore": addedScore, "username": action.user.name})}
        return newState
      } else {
        alert('Sorry. Try again!')
        return state
      }

    case `CHECK_ANSWER 2`:
      {
        const checkbox = state.ref.current.querySelector('#goal')
        console.log(checkbox)
        if (checkbox.checked) {
          const newState = {...state, pointsModalOpen: true }
          if (action.user.isLoggedIn) {
            const newTaskPoints = {...action.taskPoints}
            newTaskPoints.task2="1"
            const addedScore = Object.values(newTaskPoints).filter(current => current > 0).length*250
          axios.post(`http://localhost:3001/scores/${action.user.name}`, { "taskPoints": newTaskPoints, "addedScore": addedScore, "username": action.user.name})}
          return newState
        } else {
          alert('Check the checkbox or press escape to return')
          return state
        }
      }
    case `CHECK_ANSWER 3`: {
      const headers = state.ref.current.querySelectorAll('h1, h2, h3, h4, h5')
      const headersArray = Array.prototype.slice.call(headers)
      const correctArray = Array.prototype.slice.call(headers)
      correctArray.sort((a, b) => a.id - b.id)
      if (isEqual(headersArray, correctArray)) {
        const newState = {...state, pointsModalOpen: true }
        if (action.user.isLoggedIn) {
          const newTaskPoints = {...action.taskPoints}
          newTaskPoints.task3="1"
          const addedScore = Object.values(newTaskPoints).filter(current => current > 0).length*250
          axios.post(`http://localhost:3001/scores/${action.user.name}`, { "taskPoints": newTaskPoints, "addedScore": addedScore, "username": action.user.name})}
        return newState
      } else {
        alert('Order is not correct')
        return state
      }
    }
    case `CHECK_ANSWER 4`: {
      const images = state.ref.current.querySelectorAll('img')
      const imageArray = Array.prototype.slice.call(images)
      const idOrder = imageArray.map((current) => current.id)
      function idToNumber(id) {
        const numbers = {
          '0': '42',
          '1': '74',
          '2': '29',
          '3': '12',
          '4': '16',
          '5': '7',
          '6': '26',
          '7': '5',
          '8': '7',
          '9': '16',
        }
        return numbers[id]
      }
      const numberOrder = idOrder
        .map((current) => {
          return idToNumber(current)
        })
        .join('')
      console.log('state', idOrder)
      console.log('numbers', numberOrder)

      const numbersInput = state.ref.current.querySelector('input').value
      console.log(numbersInput)
      if (numbersInput===numberOrder) {
      const newState = {...state, pointsModalOpen: true }
      if (action.user.isLoggedIn) {
        const newTaskPoints = {...action.taskPoints}
        newTaskPoints.task4="1"
        const addedScore = Object.values(newTaskPoints).filter(current => current > 0).length*250
          axios.post(`http://localhost:3001/scores/${action.user.name}`, { "taskPoints": newTaskPoints, "addedScore": addedScore, "username": action.user.name})}
      return newState
      } else {
        alert('Wrong order. Try again!')
        return state
      }
    }
    case 'SET_SCORES':
        console.log('action', action)
        
       if (action.user.isLoggedIn) {
          axios.post(`http://localhost:3001/scores/${action.user.name}`, { "taskPoints": action.taskPoints, "username": action.user.name}).then((response, error) => {
            if (error) {
              console.log(error)
            } else {
              console.log('newScore', action.taskPoints)
            }
          })  
        }
    case 'DEFAULT':
      return { taskId: state.taskId }
  }
}
