import React, { useContext } from 'react'
import axios from 'axios'
import { UserContext } from '../contexts/userContext'

export const scoresReducer = async (state, action) => {
  switch (action.type) {
    case 'GET_SCORES':
      const scores = await axios
        .get('http://localhost:3001/scores')
        .then((response, error) => {
          if (error) {
            console.log(error)
          } else {
            return response.data.scores
          }
          return scores
        })
    case 'SET_SCORES':
      console.log('taskPointstoSet', action.taskPoints)
      console.log('action', action)
      const addedScore =
        Object.values(action.taskPoints).filter((current) => {
          return current === '1'
        }).length * 250
      console.log(addedScore)
      if (action.user.isLoggedIn) {
        await axios
          .post(`http://localhost:3001/scores/${action.user.name}`, {
            taskPoints: action.taskPoints,
            username: action.user.name,
            addedScore: addedScore,
          })
          .then((response, error) => {
            if (error) {
              console.log(error)
            } else {
              const scores = action.taskPoints
              const newScores = scores
              console.log('newScore', newScores)
            }
          })
      }
    default:
      return state
  }
}
