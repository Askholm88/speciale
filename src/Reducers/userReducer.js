import React from 'react'
import axios from 'axios'

export const userReducer = (state, action) => {
  switch (action.type) {
    case 'CREATE_USER':
      axios
        .post(`http://localhost:3001/user/signup`, {
          name: action.name,
          password: '"' + action.password + '"',
        })
        .then((response, error) => {
          if (error) {
            console.log(error)
            alert('Something went wrong!')
            return state
          } else {
            alert('User created successfully. Please log in!')
          }
        })
      return state
    case 'LOGIN':
      axios
        .post(`http://localhost:3001/user/login`, {
          name: action.name,
          password: '"' + action.password + '"',
        })
        .then((response, error) => {
          if (error) {
            console.log(error)
            alert('Something went wrong!')
            return state
          } else {
            alert('Login successful!')
            action.setOpen(false)
            console.log(response)
            state.isLoggedIn = true
            state.name = action.name
            state.addedScore = response.data.addedScore
            state.taskScores = response.data.taskScore
            state.achievements = response.data.achievements
          }
        })
      return state
    default:
      return state
  }
}
